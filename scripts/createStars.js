const createStars = (numToMake) => {
    const stars = new Array(Math.floor(numToMake)).fill(`<i class="fa fa-star" aria-hidden="true"></i>`)
    return stars;
}

export default createStars;