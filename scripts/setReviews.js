import setReviewToShow from './setReviewToShow.js';
import createStars from './createStars.js';
/* import reviewNotMutated from './fetchRequest.js'; */

let showReview = false;
/* let currentPage = null; */
/* Top centre */

let reviewsWithAttachments = [];
let show = false;



let showLoadMoreButton = true;

const setReviews = (array, currentPage = 1, imagesToShow = 6) => {
    
    const reviewsContainer = document.querySelector('.trustPilot__reviewsContainer__cards');
    const galleryImages = document.querySelector('.trustPilot__gallery__images');
    const showMoreReviews = document.querySelector('.showMoreReviews');
    const loadMoreImages = document.querySelector('.loadMoreImages');

    if(showLoadMoreButton){
        showMoreReviews.addEventListener('click', () => {
            console.log("clicked")
            setReviews(array, currentPage + 1)
        })
    }

    if(!show){
        show = true;
        array.forEach(review => {

            if(review.attachments.length > 0){
                reviewsWithAttachments = [...reviewsWithAttachments, createImageGallaryHTML(review)];
            }  

           /*  let attHtml = ''
            review.attributeRatings.forEach(attribute => {
                attHtml += `<p>${attribute.attributeName}</p>
                                <div class="trustPilot__stars">
                                        ${createStars(attribute.rating).join("")}
                                </div>`
            }) */
        })
    }
    let html = '';
   
    const perPage = 4;
   /*  const currentPage = 1; */
    const total = array.length;
   /*  const paginationCurrentPage = getButtons(total, perPage, currentPage, array); */
  
   
    const arrAfterPagination = array.slice(0 , currentPage * perPage);

    if(arrAfterPagination.length >= array.length){
        showLoadMoreButton = false;
        showMoreReviews.classList.add('displayNone');
    }
    /* left */
    arrAfterPagination.forEach(review => {

      /*   if(review.attachments.length > 0){
            reviewsWithAttachments = [...reviewsWithAttachments, createImageGallaryHTML(review)];
        }   */

        let attHtml = ''
        review.attributeRatings.forEach(attribute => {
            attHtml += `<div class="trustPilot__stars__container">
                            <p>${attribute.attributeName}</p>
                            <div class="trustPilot__stars">
                                    ${createStars(attribute.rating).join("")}
                            </div>
                        </div>`
        })



const monthNum = new Date(review.createdAt).getMonth();
const dayNum = new Date(review.createdAt).getDate();
const yearNum = new Date(review.createdAt).getFullYear();
const month = getMonthToShow(monthNum);


const companyReplyMonth = new Date(review.createdAt).getMonth();
const companyReplyDay = new Date(review.createdAt).getDate()
const companyReplyYear = new Date(review.createdAt).getFullYear();
const companyMonth = getMonthToShow(companyReplyMonth);

let dateOrdinal = (date) => {
   if (date === 1 || date === 21 || date === 31) {
        return `${date}st`;
   } else if(date === 2 || date === 22){
       return `${date}nd`;
   } else if(date === 3 || date === 23){
        return `${date}rd`;
   } else {
        return `${date}th`
   }
} 


const getTitle = (content) => {
    
    if(content.length > 50){
        const getWords = content.slice(0,50);
        const removeLastWord = getWords.split(' ').slice(0, -1).join(' ');
        return `${removeLastWord}...`;
    } else {
        return content;
    }

}
console.log(dateOrdinal(companyReplyDay))

            html += `<div class="trustPilot__reviewsContainer__card">
                 
                            <div class="trustPilot__reviewsContainer__card__left__text_name">
                            <div class="trustPilot__reviewsContainer__card__left__text_name__name"><h3>${review.consumer.displayName}</h3></div>
                            <div class="trustPilot__reviewsContainer__card__left__text_name__date">${`${month} ${dateOrdinal(dayNum)} ${yearNum}`}</div>
                     
                     
                    </div>
                    <div class="trustPilot__reviewsContainer__card__middle">
                    <div>
                      
                        <div class="trustPilot__reviewsContainer__card__middle__rating">${createStars(review.stars).join("")}</div>
                    </div> 
                  ${attHtml}
                 
                </div>
                   
                            <div class="trustPilot__reviewsContainer__card__right__text__header">
                                <h3>${getTitle(review.content)}</h3>
                            </div>
                            <div class="trustPilot__reviewsContainer__card__right__text__content">
                                
                                <p>${review.content}</p> 
                            </div>
                            ${review.attachments.length > 0 ?
                `<div class="trustPilot__reviewsContainer__card__right__text__reviewImages">
                                ${review.attachments.map(image => {
                    return `<div class="trustPilot__reviewsContainer__card__right__text__reviewImages__container"><img src=${`${image.processedFiles[1].url}`}></div>`
                }).join("")}</div>` : ''
            }
                          
                            ${review.firstCompanyComment ? `<div class="trustPilot__reviewsContainer__card__right__text__repsonse">
                                <div class="trustPilot__reviewsContainer__card__right__text__repsonse__details">
                                    <div class="trustPilot__reviewsContainer__card__right__text__repsonse__details__company"><p><strong>Response from inov-8</strong></p></div>
                                    <div class="trustPilot__reviewsContainer__card__right__text__repsonse__details__date"><p>${`${companyMonth} ${dateOrdinal(companyReplyDay)} ${companyReplyYear}`}</p></div>
                                </div>
                                <div class="trustPilot__reviewsContainer__card__right__text__repsonse__content">
                                    <p>${review.firstCompanyComment.comment}</p> 
                                </div>
                            </div>`: ''}
                      
                </div>`
  
            })

    reviewsContainer.innerHTML = html;

   
    let images = [];
    reviewsWithAttachments.forEach(review => {
        review.forEach(img => {
               /*  galleryImages.append(img);  */
               images = [ ...images, img ]
    })
    })

    if(images.length > 8){
            loadMoreImages.classList.remove('displayNone')
    } else {
        images.forEach(img => {
            galleryImages.append(img)
            console.log(77777777, "sycsds")
        })
    }
    
}



/* const getButtons = (total, perPage, currentPage = 1, array) => {

    const trustPilotPagination = document.querySelector('.trustPilot__pagination');
    trustPilotPagination.innerHTML = '';

    let numOfPages = Math.ceil(total / perPage)
   
    const arr = new Array(numOfPages)
    
   for(let i = 0; i < numOfPages; i ++){
      arr[i] = i + 1
   }

   let pagination = document.createElement('div');
  if(arr.length > 1){
  
   pagination.classList.add('trustPilot__pagination');
   trustPilotPagination.appendChild(pagination);
  }
  

   arr.forEach((arr, i) => {
        if(currentPage === i + 1){
            let div = document.createElement('div');
            div.innerHTML = `<p>${arr}</p>`;
            div.classList.add('currentPage');
            div.classList.add('pagination__div');
            pagination.appendChild(div);
            
    
            div.onclick = () => {
                console.log(arr)
            }
        } else{
            let div = document.createElement('div');
            div.classList.add('pagination__div');
            div.innerHTML = `<p>${arr}</p>`;
            pagination.appendChild(div);
    
            div.onclick = () => {
            
                currentPage = arr;
               
                setReviews(array, arr) 
            }
        }
       

   })

   return currentPage;
}
 */
const createImageGallaryHTML = (review) => {
  
    const modal = document.querySelector('#trustPilotModal');
    
   
      const imgTest = review.attachments.map(image => {

        let imageToDisplay = image.processedFiles[1].url;
        
      let imgDiv = document.createElement('div');
        imgDiv.classList.add('imageToPress__div');
      

          let img = document.createElement('img');
          img.classList.add('imageToPress');
          img.setAttribute("src", `${imageToDisplay}`);
          imgDiv.append(img);


            img.onclick = () => {
                modal.classList.remove('displayNone')
                setReviewToShow(review, showReview)
            }
           return imgDiv;
        })
  
    return imgTest
}



const getMonthToShow = (month) => {
    let selectedMonth = ''
  
    switch (month) {
        case 0:
            selectedMonth = "Jan"
            break;
        case 1:
            selectedMonth = "Feb"
            break;
        case 2:
            selectedMonth = "Mar"
            break;
        case 3:
            selectedMonth = "April"
            break;
        case 4:
            selectedMonth = "May"
            break;
        case 5:
            selectedMonth = "June"
            break;
        case 6:
            selectedMonth = "July"
            break;
        case 7:
            selectedMonth = "Aug"
            break;
        case 8:
            selectedMonth = "Sep"
            break;
        case 9:
            selectedMonth = "Oct"
            break;
        case 10:
            selectedMonth = "Nov"
            break;
        case 11:
            selectedMonth = "Dec"
            break;
      
        /*  default: */
        // code block
    }

    return selectedMonth;
}

export default setReviews;