import createStars from './createStars.js';

/* Top right */
const setAttributeSummaries = (data) => {
    const container = document.querySelector(".trustPilot__statistics__right__container");
    let html = '';
    data.attributes.forEach(attribute => {
        const stars = createStars(Math.floor(attribute.average));
        html += `<div class="trustPilot__statistics__right__container__container"> 
        <p>${attribute.name} - ${attribute.average}/5</p> 
        <div class="trustPilot__stars">
            ${stars.map(star => star).join("")}
        </div> 
        </div>`
    })
    container.innerHTML = html;
}

export default setAttributeSummaries;