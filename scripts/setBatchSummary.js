import createStars from './createStars.js';

/* Top left */
const setBatchSummary = (data) => {
    const statistics = document.querySelector('.trustPilot__statistics__left__container');
    const introSummary = document.querySelector('.trustPilot__starRating__summary');
    const starRating = document.querySelector('.trustPilot__starRating__stars');
    const keys = Object.keys(data.numberOfReviews).reverse();
    let html = '';
    keys.forEach(key => {
        if (key !== "total") {
            html += `<div class="trustPilot__statistics__left__container__container">
                <div class="trustPilot__statistics__left__container__container__numOfStars">
                    <p>${returnNumber(key)} stars</p>
                </div>
                <div class="productInfo__stat__bar__percent" style="background-color: #E8E8E8">
                    <div style="background-color: #000; width: ${((data.numberOfReviews[key] / data.numberOfReviews.total) * 100).toFixed(0)}%"></div>
                </div>
                <div class="trustPilot__statistics__left__container__container__amountToShow">
                    <p>${data.numberOfReviews[key]}</p>
                </div>
            </div>`
        }
    })
    const avgStars = createStars(Math.floor(data.starsAverage)).join("")
    introSummary.innerHTML = `<h3>${data.numberOfReviews.total} Reviews (Avg. ${data.starsAverage}/5)</h3>`
    statistics.innerHTML = html;
    starRating.innerHTML = avgStars;
}


const returnNumber = (wording) => {
    let num = ''
    switch (wording) {
        case "oneStar":
            num = "1"
            break;
        case "twoStars":
            num = "2"
            break;
        case "threeStars":
            num = "3"
            break;
        case "fourStars":
            num = "4"
            break;
        case "fiveStars":
            num = "5"
            break;
        /*  default: */
        // code block
    }
    return num;
}



export default setBatchSummary;