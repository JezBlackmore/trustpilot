import setReviews from './setReviews.js';

import fetchRequest from './fetchRequest.js';


fetchRequest();

const fetchByStars = (numOfStars) => {
    fetch(`https://api.trustpilot.com/v1/product-reviews/business-units/578fa44b0000ff0005928cb5/reviews?sku=TRUSTPILOT_SKU_VALUE_16322&apikey=2k2brkYaNNRUgb7p3VWmS9x5cGBIVmXU&&stars=${numOfStars}`)
    .then(response => response.json())
    .then(data => {
        if(data.productReviews.length > 0){
            setReviews(data.productReviews);
        } else {
            setReviews([]);
            const div = document.querySelector('.trustPilot__reviewsContainer__cards');
            const childToAdd = `<p>No reviews match your search</p>`;
            div.innerHTML = childToAdd
           
            console.log("Nothing to show")
        }
    })
    .catch(err => console.log(err));
}

const searchByStars = document.getElementById('rating');

searchByStars.addEventListener('change', (e) => {
    const images = document.querySelector('.trustPilot__gallery__images');
    images.innerHTML = '';
 
    fetchByStars(`${e.target.value}`)
})

















