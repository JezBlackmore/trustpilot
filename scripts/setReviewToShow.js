import createStars from './createStars.js';

let showReview = false;
const trustPilotModal = document.querySelector('#trustPilotModal');
const trustPilotModalInner = document.querySelector('.trustPilotModal__inner');
const primaryContainer = document.querySelector('#primaryContainer');

trustPilotModal.addEventListener('click', (e) => {
    showReview = false;
    trustPilotModal.classList.add('displayNone')
})
trustPilotModalInner.addEventListener('click', (e) => {
    e.stopPropagation();
}) 



const  setReviewToShow = (review) => {

  /*   const test = document.querySelector('.trustPilotModal__inner') */
 
/* console.log(trustPilotModalInner) */
/* trustPilotModalInner.removeChild(test)  */

  /*   const toRemove = document.querySelector(".trustPilotModal__inner__test")     

    if(toRemove.hasChildNodes()){
        trustPilotModalInner.removeChild(toRemove)
    } */

const monthNum = new Date(review.createdAt).getMonth();
const dayNum = new Date(review.createdAt).getDate();
const yearNum = new Date(review.createdAt).getFullYear();
const month = getMonthToShow(monthNum);


const companyReplyMonth = new Date(review.createdAt).getMonth();
const companyReplyDay = new Date(review.createdAt).getDate()
const companyReplyYear = new Date(review.createdAt).getFullYear();
const companyMonth = getMonthToShow(companyReplyMonth);

let dateOrdinal = (date) => {
   if (date === 1 || date === 21 || date === 31) {
        return `${date}st`;
   } else if(date === 2 || date === 22){
       return `${date}nd`;
   } else if(date === 3 || date === 23){
        return `${date}rd`;
   } else {
        return `${date}th`
   }
} 


const getTitle = (content) => {
    
    if(content.length > 50){
        const getWords = content.slice(0,50);
        const removeLastWord = getWords.split(' ').slice(0, -1).join(' ');
        return `${removeLastWord}...`;
    } else {
        return content;
    }

}

    const primaryImage = review.attachments[0].processedFiles[1].url;


    parent = document.createElement('div');
    parent.classList.add('trustPilotModal__inner__test');

    let content = document.createElement('div');
    content.classList.add('trustPilotModal__inner__gallery');
    parent.appendChild(content);

    let div2 = document.createElement('div');
    div2.setAttribute("id", "primaryContainer");
    div2.classList.add('trustPilotModal__inner__gallery__image');
    content.appendChild(div2);

    let img1 = document.createElement('img');
    img1.setAttribute("src", primaryImage);
    div2.appendChild(img1);


    let div1 = document.createElement('div');
    div1.classList.add('trustPilotModal__inner__gallery__right');
    div2.appendChild(div1);
    div1.innerHTML = `<i class="fa fa-chevron-left" aria-hidden="true"></i>`;

    div1.onclick = () => {
        console.log("Clicked Right")
    }

    let div3 = document.createElement('div');
    div3.classList.add('trustPilotModal__inner__gallery__left');
    div2.appendChild(div3);
    div3.innerHTML = `<i class="fa fa-chevron-right" aria-hidden="true"></i>`;
    
    div3.onclick = () => {
        console.log("Clicked Left")

    }

    if(showReview){

        let attHtml = ''
        review.attributeRatings.forEach(attribute => {
            attHtml += `
            <div class="trustPilotModal__stars">
                <p>${attribute.attributeName}</p>
                <div class="trustPilot__stars">
                    ${createStars(attribute.rating).join("")}
                </div>
            </div>`
        })


        let contentInner = document.createElement('div');
        contentInner.classList.add('trustPilotModal__inner__text');
        parent.append(contentInner);

        let div6 = document.createElement('div');
        div6.classList.add('trustPilotModal__inner__text__button');
        contentInner.appendChild(div6);

        let div6button = document.createElement('button');
        div6button.classList.add('trustPilotButton');
        div6button.innerText = `Hide Review`;
        div6.append(div6button);

        let div4 = document.createElement('div');
        div4.classList.add('trustPilotModal__inner__text__stars');
        contentInner.appendChild(div4);

        div4.innerHTML = createStars(review.stars).join("");
        
        

        let div6a = document.createElement('div');
        div6a.classList.add('trustPilotModal__inner__text__review');
        contentInner.appendChild(div6a);
    
        div6a.innerHTML = `<h3>${getTitle(review.content)}</h3>`;


    let div7 = document.createElement('div');
    div7.classList.add('trustPilotModal__inner__text__review');
    contentInner.appendChild(div7);

    div7.innerHTML = `<p>${review.content}</p>`;


    let div5 = document.createElement('div');
    div5.classList.add('trustPilotModal__inner__text__review');
    contentInner.appendChild(div5); 

    div5.innerHTML =   `<div class="trustPilot__reviewsContainer__card__right__text_name">
    <div class="trustPilotModal__reviewsContainer__card__text_name__name"><h3>${review.consumer.displayName}</h3></div>
    <div class="trustPilotModal__reviewsContainer__card__text_name__date"><p>${`${month} ${dateOrdinal(dayNum)} ${yearNum}`}</p></div>
</div>`

        let div8 = document.createElement('div');
        contentInner.appendChild(div8);
        
        div8.innerHTML =  `<div class="trustPiloModal__reviewsContainer__card__left">
            ${attHtml}
        </div>`
 
        
        if(review.attachments.length > 0){

            let div9 = document.createElement('div');
            div9.classList.add('trustPilot__reviewsContainer__card__right__text__reviewImages');
            contentInner.appendChild(div9);

            review.attachments.forEach((image, index) => {
                    let divImage = document.createElement('div');
                    divImage.classList.add('trustPilot__reviewsContainer__card__right__text__reviewImages__container');
                    divImage.innerHTML = `<img src=${`${image.processedFiles[1].url}`}>`
                    div9.appendChild(divImage);

                    divImage.onclick = (e) => {
                        console.log("Clicked Image 2")
                       /*  console.log(image.processedFiles[index].url) */
                     
                       document.querySelector('#primaryContainer').innerHTML = `<img src=${ image.processedFiles[index].url} />`
                    }
                })

              /*   div9.innerHTML = innerHtmlToShow; */
        } 
          
        

        div6button.onclick = () => {
            showReview = false;
            console.log("Click")
            setReviewToShow(review);
        }
} else{
    let contentInner = document.createElement('div');
    contentInner.classList.add('trustPilotModal__inner__text');
    parent.append(contentInner);

    let div6 = document.createElement('div');
    div6.classList.add('trustPilotModal__inner__text__button');
    contentInner.appendChild(div6);

    let div6button = document.createElement('button');
    div6button.classList.add('trustPilotButton');
    div6button.innerText = `Show Review`;
    div6.append(div6button);

    div6button.onclick = () => {
        showReview = true;
        console.log("Click")
        setReviewToShow(review);
    }

    let div4 = document.createElement('div');
    div4.classList.add('trustPilotModal__inner__text__stars');
    contentInner.appendChild(div4);

    div4.innerHTML = createStars(review.stars).join("");

    let div5 = document.createElement('div');
    div5.classList.add('trustPilotModal__inner__text__review');
    contentInner.appendChild(div5);

    div5.innerHTML = `<h3>${getTitle(review.content)}</h3>`;


 

}



trustPilotModalInner.appendChild(parent)

    if(trustPilotModalInner.childNodes.length > 1){
        trustPilotModalInner.removeChild(trustPilotModalInner.childNodes[0]);
    }
}



const getMonthToShow = (month) => {
    let selectedMonth = ''
  
    switch (month) {
        case 0:
            selectedMonth = "Jan"
            break;
        case 1:
            selectedMonth = "Feb"
            break;
        case 2:
            selectedMonth = "Mar"
            break;
        case 3:
            selectedMonth = "April"
            break;
        case 4:
            selectedMonth = "May"
            break;
        case 5:
            selectedMonth = "June"
            break;
        case 6:
            selectedMonth = "July"
            break;
        case 7:
            selectedMonth = "Aug"
            break;
        case 8:
            selectedMonth = "Sep"
            break;
        case 9:
            selectedMonth = "Oct"
            break;
        case 10:
            selectedMonth = "Nov"
            break;
        case 11:
            selectedMonth = "Dec"
            break;
      
        /*  default: */
        // code block
    }

    return selectedMonth;
}

export default setReviewToShow;