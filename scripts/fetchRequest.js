import setAttributeSummaries from './setAttributeSummaries.js';
import setBatchSummary from './setBatchSummary.js';
import setReviews from './setReviews.js';

const fetchRequest = () => {

    fetch(`https://api.trustpilot.com/v1/product-reviews/business-units/578fa44b0000ff0005928cb5/reviews?sku=TRUSTPILOT_SKU_VALUE_16322&apikey=2k2brkYaNNRUgb7p3VWmS9x5cGBIVmXU&`)
    .then(response => response.json())
    .then(data => {
        console.log(data)
        setReviews(data.productReviews);
        
    
    })
    .catch(err => console.log(err));

    fetch('https://api.trustpilot.com/v1/product-reviews/business-units/578fa44b0000ff0005928cb5/batch-summaries?apikey=2k2brkYaNNRUgb7p3VWmS9x5cGBIVmXU', {
    method: "POST",
    body: JSON.stringify({
        "skus": [
            "TRUSTPILOT_SKU_VALUE_16322"
        ]
    }),
    headers: {
        "Content-Type": "application/json"
    }
    }).then(response => response.json())
    .then(data => {

        setBatchSummary(data.summaries[0])
    })
    .catch(err => console.log(err))

    fetch('https://api.trustpilot.com/v1/product-reviews/business-units/578fa44b0000ff0005928cb5/attribute-summaries?apikey=2k2brkYaNNRUgb7p3VWmS9x5cGBIVmXU', {
    method: "POST",
    body: JSON.stringify({
        "skus": [
            "TRUSTPILOT_SKU_VALUE_16322"
        ]
    }),
    headers: {
        "Content-Type": "application/json"
    }
    })
    .then(response => response.json())
    .then(data => {
        setAttributeSummaries(data.summaries[0]);
    })
    .catch(err => console.log(err))

}


export default fetchRequest;